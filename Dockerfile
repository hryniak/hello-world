FROM ubuntu:latest
MAINTAINER Wacek Cycko <wacek@cycko.pl>

# Set environment
ENV JAVA_HOME /opt/jdk
ENV PATH ${PATH}:${JAVA_HOME}/bin
ENV JAVA_PACKAGE server-jre

RUN apt-get update &&\
	apt-get -y  install wget &&\
	apt-get -y install openjdk-8-jdk &&\
	java -version

# Install devops tools
RUN apt-get -y install mc &&\
	apt-get -y install vim &&\
	apt-get -y install screen

#--------------
ENV TOMCAT_VERSION_MAJOR 7
ENV TOMCAT_VERSION_FULL  7.0.55
ENV CATALINA_HOME /opt/tomcat

RUN mkdir -p $JAVA_HOME &&\
	ln -s /usr/bin/ $JAVA_HOME 


RUN wget -q --show-progress https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_VERSION_MAJOR}/v${TOMCAT_VERSION_FULL}/bin/apache-tomcat-${TOMCAT_VERSION_FULL}.tar.gz &&\
  	wget -q --show-progress https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_VERSION_MAJOR}/v${TOMCAT_VERSION_FULL}/bin/apache-tomcat-${TOMCAT_VERSION_FULL}.tar.gz.md5 &&\
	md5sum -c apache-tomcat-${TOMCAT_VERSION_FULL}.tar.gz.md5 &&\
  	gunzip -c apache-tomcat-${TOMCAT_VERSION_FULL}.tar.gz | tar -xf - -C /opt &&\
	rm -f apache-tomcat-${TOMCAT_VERSION_FULL}.tar.gz apache-tomcat-${TOMCAT_VERSION_FULL}.tar.gz.md5 &&\
  	ln -s /opt/apache-tomcat-${TOMCAT_VERSION_FULL} ${CATALINA_HOME} &&\
  	rm -rf /opt/tomcat/webapps/examples /opt/tomcat/webapps/docs &&\
  	rm -rf /var/cache/apk/*

COPY src/tomcat-users.xml /opt/tomcat/conf/tomcat-users.xml

# Launch Tomcat on startup
CMD ${CATALINA_HOME}/bin/catalina.sh run
